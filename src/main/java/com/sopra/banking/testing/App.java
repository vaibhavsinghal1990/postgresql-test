package com.sopra.banking.testing;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Connecting to DB
 *
 */
public class App 
{
	public static void main( String[] args ) throws SQLException
	{
		System.out.println("-------- PostgreSQL "
				+ "JDBC Connection Testing ------------");
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			System.out.println("Where is your PostgreSQL JDBC Driver? "
					+ "Include in your library path!");
			e.printStackTrace();
			return;
		}//EndTryCatch
		System.out.println("PostgreSQL JDBC Driver Registered!");
		Connection connection = null;
		try {
			connection = DriverManager.getConnection(
					"jdbc:postgresql://test-postgresql:5432/pgdb", "pguser",
					"pgpassword");
		} catch (SQLException e) {
			System.out.println("Connection Failed! Check output console");
			e.printStackTrace();
			return;
		}//EndTryCatch
        
		create(connection);
		select(connection);
		insert(connection);
		select(connection);
		//delete(connection);
		//select(connection);

		connection.close();
	}//EndMethod
	
	public static void create(Connection connection) throws SQLException{
		String queryinsert = " CREATE TABLE sopra(id serial PRIMARY KEY,"
				+ "name VARCHAR (50) UNIQUE NOT NULL,age VARCHAR (50) NOT NULL,address VARCHAR (355) UNIQUE NOT NULL,salary serial NOT NULL)";
		// create the mysql create preparedstatement
		PreparedStatement preparedStmt = connection.prepareStatement(queryinsert);
		System.out.println("Create table executed");	
		// execute the preparedstatement
		preparedStmt.execute();
	}
	public static void insert(Connection connection) throws SQLException{
		String queryinsert = " insert into sopra (id, name, age, address, salary)"
				+ " values (?, ?, ?, ?, ?)";
		// create the mysql insert preparedstatement
		PreparedStatement preparedStmt = connection.prepareStatement(queryinsert);
		preparedStmt.setInt (1, 1234);
		preparedStmt.setString (2, "Ram");
		preparedStmt.setInt  (3, 25);
		preparedStmt.setString(4, "UP");
		preparedStmt.setInt (5, 5000);
		System.out.println("Insert executed");	
		// execute the preparedstatement
		preparedStmt.execute();
	}


	public static void delete(Connection connection) throws SQLException{

		String queryDelete = "delete from sopra where id = ?";
		PreparedStatement preparedStmtDel = connection.prepareStatement(queryDelete);
		preparedStmtDel.setInt(1, 1234);
		preparedStmtDel.execute(); 
		System.out.println("Delete executed");		
	}

	public static void select(Connection connection) throws SQLException {
		if (connection != null) {
			System.out.println("Select table details");		
			String query = "SELECT * FROM sopra";
			// create the java statement
			Statement st = connection.createStatement();		      
			// execute the query, and get a java resultset
			ResultSet rs = st.executeQuery(query);		      
			// iterate through the java resultset
			while (rs.next())
			{
				int id = rs.getInt("id");
				String firstName = rs.getString("name");
				int age = rs.getInt("age");
				String address = rs.getString("address").trim();
				int salary = rs.getInt("salary");

				// print the results
				System.out.println(id + "," + firstName +","+ age +","+ address +"," + salary );
			}//EndWhileLoop 
		}else {
			System.out.println("Not able to select");
		}
	}
}//EndClass