package com.sopra.banking.testing;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MongoTest {
	public static void main( String[] args ) throws SQLException
	{
		System.out.println("-------- PostgreSQL "
				+ "JDBC Connection Testing ------------");
		try {
			Class.forName("mongodb.jdbc.MongoDriver");
		} catch (ClassNotFoundException e) {
			System.out.println("Where is your MongoDB JDBC Driver? "
					+ "Include in your library path!");
			e.printStackTrace();
			return;
		}//EndTryCatch
		System.out.println("Mongo JDBC Driver Registered!");
		Connection connection = null;
		try {
			connection = DriverManager.getConnection(
					"jdbc:mongo://172.26.174.67:5432/admin", "myUserAdmin",
					"123abc");
		} catch (SQLException e) {
			System.out.println("Connection Failed! Check output console");
			e.printStackTrace();
			return;
		}//EndTryCatch
	}
}
